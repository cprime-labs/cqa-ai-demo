# cqa-ai-demo

# Build Status

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)



## NatWest API Links

[Bank of APIs](https://www.bankofapis.com/)

[Developer Sandbox](https://developer.sandbox.natwest.com/dashboard)
