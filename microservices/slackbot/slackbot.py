import os
from dotenv import load_dotenv
from slack_bolt import App
from slack_bolt.adapter.socket_mode import SocketModeHandler
from langchain import OpenAI, LLMChain
from langchain.chains.conversation.memory import ConversationBufferWindowMemory
from langchain import PromptTemplate


# Set API credentials
load_dotenv()
SLACK_BOT_TOKEN = os.environ["SLACK_BOT_TOKEN"]
SLACK_APP_TOKEN = os.environ["SLACK_APP_TOKEN"]
OPENAI_API_KEY = os.environ["CPRIME_OPENAI_API_KEY"]

# Initializes app with bot token
app = App(token=SLACK_BOT_TOKEN)

# Create prompt template
prompt_template = """You are a helpful assistant. If you don't know the answer to a question, just say that you don't know, don't try to make up an answer.

{history}

Human: {human_input}

AI:"""

prompt = PromptTemplate(
    input_variables=["history", "human_input"], template=prompt_template
)

# LLMChain to handle conversations
chatgpt_chain = LLMChain(
    llm=OpenAI(openai_api_key=OPENAI_API_KEY, temperature=0),
    prompt=prompt,
    verbose=True,
    memory=ConversationBufferWindowMemory(k=2),
)


# Message handler for Slack
@app.message(".*")
def message_handler(message, say):
    print(message)
    chatgpt_chain.prompt = prompt
    output = chatgpt_chain.predict(human_input=message["text"])
    say(output)


# Start app
if __name__ == "__main__":
    SocketModeHandler(app, SLACK_APP_TOKEN).start()
