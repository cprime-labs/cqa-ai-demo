package com.cprime.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Boot Aplication to serve the gateway REST microservice.
 *
 * @author @devopskev
 */
@SpringBootApplication
@SuppressWarnings("PMD.UseUtilityClass")
public class GatewayApplication {

  /** Default contructor. */
  public static void main(final String[] args) {
    SpringApplication.run(GatewayApplication.class, args);
  }
}
